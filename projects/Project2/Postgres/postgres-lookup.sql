CREATE TABLE look_up
(patient_id int NOT NULL,
dr_id int NOT NULL,
FOREIGN KEY (patient_id) REFERENCES patients(patient_id) match full,
FOREIGN KEY (dr_id) REFERENCES doctors(dr_id) match full);