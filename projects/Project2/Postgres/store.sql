CREATE OR REPLACE FUNCTION add_order(
  patient_id int,
  dr_id int)

RETURNS void AS $$
DECLARE
  is_dr boolean := false;
  patient_name TEXT;
  doctor TEXT;
  temp_row LookUp%rowtype;
BEGIN
  FOR temp_row IN 
    SELECT patient_id,dr_id
    FROM lookup l
    WHERE l.patient_id=dr_id;
  LOOP 
    --Here I should have a loop where I check either patient is doctor or not. which I have the lookup table of 35 doctors 
    ---being patient at the same time. So if patient_id matches with any of these dr_ids on that look_up table
    ---then patient can't be doctor of themselves
  END LOOP;

  -- Note: this is a notice, not an error as in some programming languages
  RAISE NOTICE 'You can not be doctor of yourself %', dr_id;


  RETURN did_insert;
END;
$$ LANGUAGE plpgsql;