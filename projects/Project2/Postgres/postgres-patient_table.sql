CREATE TABLE patients
(patient_id serial PRIMARY KEY,
patient_name VARCHAR(55) NOT NULL,
illness_id int ,
is_doctor BOOLEAN NOT NULL,
dr_id int NOT NULL,
FOREIGN KEY (illness_id) REFERENCES illness(illness_id) match full,
FOREIGN KEY (dr_id) REFERENCES doctors(dr_id) match full);