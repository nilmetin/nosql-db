CREATE TABLE illness
(illness_id serial PRIMARY KEY,
illness_name VARCHAR(55) NOT NULL,
treatment_id int,
FOREIGN KEY (treatment_id) REFERENCES treatment(treatment_id) match full);