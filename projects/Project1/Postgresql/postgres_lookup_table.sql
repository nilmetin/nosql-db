CREATE TABLE LookUp
(recipe_id int,
inventory_id int,
lookup_quantity int NOT NULL,
PRIMARY KEY (recipe_id, inventory_id),
FOREIGN KEY (recipe_id) REFERENCES recipes(recipe_id) match full,
FOREIGN KEY (inventory_id) REFERENCES inventory(inventory_id) match full);