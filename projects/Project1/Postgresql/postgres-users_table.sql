CREATE TABLE users 
(userid serial PRIMARY KEY,
first_name VARCHAR(55) NOT NULL,
last_name VARCHAR(55) NOT NULL,
email text unique, 
phone_number int unique,
address VARCHAR(130),
city VARCHAR(30),
state VARCHAR(30),
zipcode varchar(9) CHECK (zipcode <> ''));