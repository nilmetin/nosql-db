CREATE TABLE recipes
(recipe_id serial PRIMARY KEY,
recipe_name text unique NOT NULL,
description_recipe text,
instructions text NOT NULL);