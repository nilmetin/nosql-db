CREATE TABLE inventory
(inventory_id serial PRIMARY KEY,
ingredient_name text unique,
description_inventory text,
inventory_quantity int NOT NULL);