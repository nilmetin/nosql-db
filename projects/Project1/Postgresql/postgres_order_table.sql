CREATE TABLE orders
(order_id serial PRIMARY KEY,
userid int,
order_time timestamp NOT NULL DEFAULT NOW(),
recipe_id int ,
FOREIGN KEY (userid) REFERENCES users(userid) match full,
FOREIGN KEY (recipe_id) REFERENCES recipes(recipe_id) match full);