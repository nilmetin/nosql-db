CREATE OR REPLACE FUNCTION add_order(
  userid int,
  recipe_id int)

RETURNS void AS $$
DECLARE
  did_insert boolean := false;
  inventory_quantity boolean := true;
  recipe_id integer;
  temp_row LookUp%rowtype;
BEGIN
  FOR temp_row IN 
    SELECT recipe_id,inventory_id,quantity
    FROM LookUp l
    WHERE l.recipe_id=recipe_id;
  LOOP 
    SELECT inventory.inventory_quantity INTO inv_count
    FROM inventory WHERE temp_row.inventory_id=inventory.inventory_id;

    IF temp_row.quantity > inv_count THEN inventory_quantity=false;
    ELSE UPDATE inventory SET inventory.inventory_quantity = inventory.inventory_quantity - recipe.quantity;

    END IF;
  END LOOP;

  -- Note: this is a notice, not an error as in some programming languages
  RAISE NOTICE 'There is not enough ingredient %', inventory_id;

  INSERT INTO orders (id, order_time, recipe_id)
  VALUES (id, order_time, recipe_id);

  RETURN did_insert;
END;
$$ LANGUAGE plpgsql;