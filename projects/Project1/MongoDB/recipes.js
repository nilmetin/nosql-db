db.recipe.insert({recipe_name: 'pizza1',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pepperoni']})
WriteResult({ "nInserted" : 1 })

db.recipe.insert({recipe_name: 'pizza2',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Bacon']})
WriteResult({ "nInserted" : 1 })

db.recipe.insert({recipe_name: 'pizza3',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Ham']})


db.recipe.insert({recipe_name: 'pizza4',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', ' Sausage']})

db.recipe.insert({recipe_name: 'pizza5',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Chicken']})

db.recipe.insert({recipe_name: 'pizza6',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Peppers']})

db.recipe.insert({recipe_name: 'pizza7',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Onion']})

db.recipe.insert({recipe_name: 'pizza8',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Olives']})

db.recipe.insert({recipe_name: 'pizza9',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Spinach']})

db.recipe.insert({recipe_name: 'pizza10',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pineapple']})

db.recipe.insert({recipe_name: 'pizza11',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Green Olives']})

db.recipe.insert({recipe_name: 'pizza12',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Tomato']})

db.recipe.insert({recipe_name: 'pizza13',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Mushrooms']})

db.recipe.insert({recipe_name: 'pizza14',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Jalapeno']})

db.recipe.insert({recipe_name: 'pizza15',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Garlic']})

db.recipe.insert({recipe_name: 'pizza16',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Avacado']})

db.recipe.insert({recipe_name: 'pizza17',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pepperoni', 'Onion' ]})

db.recipe.insert({recipe_name: 'pizza18',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pepperoni', 'Spinach']})

db.recipe.insert({recipe_name: 'pizza19',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pepperoni', 'Mushrooms']})

db.recipe.insert({recipe_name: 'pizza20',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pepperoni', 'Garlic']})

db.recipe.insert({recipe_name: 'pizza21',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pepperoni', 'Pineapple']})

db.recipe.insert({recipe_name: 'pizza22',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pepperoni', 'Olives']})

db.recipe.insert({recipe_name: 'pizza23',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pepperoni', 'Jalapeno']})

db.recipe.insert({recipe_name: 'pizza24',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pepperoni', 'ham']})

db.recipe.insert({recipe_name: 'pizza25',description_recipe: 'Pizza', instructions: {step1: 'Get the dough', step2: 'pour the sauce', step3: 'Add cheese', step4: 'Add the topings', step5: 'Bake'}, rec_ingredients: ['Dough', 'Sauce', 'Cheese', 'Pepperoni', 'tomato']})


