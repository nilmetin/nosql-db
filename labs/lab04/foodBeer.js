>
db.towns.find({name: {'$regex': 'e', '$options': 'i'}, "famousFor": {'$regex': 'beer|food'}})
{
    "_id":ObjectId("5d24fb41f4c4a9e3454d54be"),
    "name":"New York",
    "population":22200000,
    "lastCensus":ISODate("2016-07-01T00:00:00Z"),
    "famousFor":["the MOMA", "food", "Derek Jeter"],
    "mayor":
    {
        "name":"Bill de Blasio",
        "party":"D"
    }
}

