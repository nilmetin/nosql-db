>
db.towns.find({name: {'$regex': 'new', '$options': 'i'}})

/* OR */

> db.towns.find({"name": {$regex: /new/i}});