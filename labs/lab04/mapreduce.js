>
map = function () {
    emit({longitude: Math.floor(this.location.longitude)}, {count: 1});
}

> reduce = function (key, values) {
    var total = 0;
    for (var i = 0; i < values.length; i++) {
        total += values[i].count;
    }
    return {count: total};
}

> results = db.runCommand({mapReduce: 'cities', map: map, reduce: reduce, out: 'cities.report'})


    > db.cities.report.find()

/*
{ "_id" : { "longitude" : -180 }, "value" : { "count" : 1 } }
{ "_id" : { "longitude" : -177 }, "value" : { "count" : 2 } }
{ "_id" : { "longitude" : -176 }, "value" : { "count" : 3 } }
{ "_id" : { "longitude" : -175 }, "value" : { "count" : 2 } }
{ "_id" : { "longitude" : -174 }, "value" : { "count" : 2 } }
{ "_id" : { "longitude" : -173 }, "value" : { "count" : 2 } }
{ "_id" : { "longitude" : -172 }, "value" : { "count" : 17 } }
{ "_id" : { "longitude" : -171 }, "value" : { "count" : 11 } }
{ "_id" : { "longitude" : -170 }, "value" : { "count" : 1 } }
{ "_id" : { "longitude" : -167 }, "value" : { "count" : 2 } }
{ "_id" : { "longitude" : -166 }, "value" : { "count" : 1 } }
{ "_id" : { "longitude" : -163 }, "value" : { "count" : 1 } }
{ "_id" : { "longitude" : -162 }, "value" : { "count" : 1 } }
{ "_id" : { "longitude" : -160 }, "value" : { "count" : 19 } }
{ "_id" : { "longitude" : -159 }, "value" : { "count" : 21 } }
{ "_id" : { "longitude" : -158 }, "value" : { "count" : 25 } }
{ "_id" : { "longitude" : -157 }, "value" : { "count" : 14 } }
{ "_id" : { "longitude" : -156 }, "value" : { "count" : 22 } }
{ "_id" : { "longitude" : -155 }, "value" : { "count" : 5 } }
{ "_id" : { "longitude" : -153 }, "value" : { "count" : 1 } }
Type "it" for more */