/* To create this new database, I just used the below command which automatically switched me to go in there */
/* Of course MongoDB doesn't really create it until we insert data into it */

>
use
blogger

/* and basically running below command created my collection in this db with all the information that is specified in it */

> db.articles.insert({
    author_name: "Hakan Menguc",
    email: "hakan.menguc@gmail.com",
    creation_date: ISODate("2016-07-01"),
    text: "Ben Ney'im"
})
WriteResult({"nInserted": 1})

/* Let's see if it exist */

> db.articles.find()
{
    "_id"
:
    ObjectId("5d30ebe5136ea1d40fcd9b41"), "author_name"
:
    "Hakan Menguc", "email"
:
    "hakan.menguc@gmail.com", "creation_date"
:
    ISODate("2016-07-01T00:00:00Z"), "text"
:
    "Ben Ney'im"
}

