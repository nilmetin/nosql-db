SELECT * FROM venues WHERE street_address ILIKE '%ave%';

/*This query searches for street_address in venues and outputs all the information(whole row) if 
street_addresses has the string 'ave' in it. wild card % is used before and after so string 
'ave' can be anywhere in street_address*/