

CREATE RULE capture_delete AS ON DELETE TO venues DO INSTEAD UPDATE venues SET active = '0' WHERE venues.venue_id = old.venue_id;