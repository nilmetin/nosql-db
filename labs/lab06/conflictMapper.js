function(doc) {
(doc.albums || []).forEach(function(album){
    (album.tracks || []).forEach(function(track){
    (track.tags || []).forEach(function(tag){
        (tag.conflict || []).forEach(function(conflict){
        emit(conflict.revision, doc._id);
        });
    });
    });
});
}